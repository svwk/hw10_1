FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /src
ENV ASPNETCORE_URLS http://*:5000

COPY . ./
RUN dotnet restore

RUN dotnet build "Otus.Teaching.PromoCodeFactory.sln" -c Release -o /src/build

WORKDIR /src/Otus.Teaching.PromoCodeFactory.WebHost
RUN dotnet publish -c release -o /app 

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS runtime
WORKDIR /app
EXPOSE 5000
EXPOSE 5001

ENV ASPNETCORE_URLS http://*:5000
ENV TZ=Europe/Moscow

COPY --from=build /app ./
CMD ["dotnet", "Otus.Teaching.PromoCodeFactory.WebHost.dll"]