﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactor.MVC.Models
{
    public class PromocodeViewModel
    {
        public PromocodeViewModel()
        {
        }

        public PromocodeViewModel(PromoCode promoCode)
        {
            Id = promoCode.Id;
            Code = promoCode.Code;
            ServiceInfo = promoCode.ServiceInfo;
            BeginDate = promoCode.BeginDate;
            EndDate = promoCode.EndDate;
            PartnerName = promoCode.PartnerName;
            PartnerManager = promoCode.PartnerManagerId;
            Preference = promoCode.PreferenceId;
        }

        [Display(Name = "Идентификационный номер")]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Поле '{0}' должно быть заполнено")]
        [StringLength(20, MinimumLength = 5, ErrorMessage = "Длина строки должна быть от 5 до 20 символов")]
        [Display(Name = "Промокод")]
        public string Code { get; set; }

        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 50 символов")]
        [Display(Name = "Информация")]
        public string ServiceInfo { get; set; }

        [Required(ErrorMessage = "Поле '{0}' должно быть заполнено")]
        [Display(Name = "Дата создания")]
        public DateTime BeginDate { get; set; }

        [Required(ErrorMessage = "Поле '{0}' должно быть заполнено")]
        [Display(Name = "Дата окончания")]
        public DateTime EndDate { get; set; }

        [Required(ErrorMessage = "Поле '{0}' должно быть заполнено")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 50 символов")]
        [Display(Name = "Имя партнера")]
        public string PartnerName { get; set; }

        [Required(ErrorMessage = "Поле '{0}' должно быть заполнено")]
        [Display(Name = "Ид менеджера")]
        public Guid PartnerManager { get; set; }

        [Required(ErrorMessage = "Поле '{0}' должно быть заполнено")]
        [Display(Name = "Ид предпочтения")]
        public Guid Preference { get; set; }
    }
}