﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactor.MVC.Models
{
    public class CustomerFullViewModel
    {
        public CustomerFullViewModel(Customer customer)
        {
            Id = customer.Id;
            FirstName = customer.FirstName;
            LastName = customer.LastName;
            Email = customer.Email;
            Preferences = new List<string>();
            foreach (var item in customer.Preferences)
            {
                Preferences.Add(item.Preference.Name);
            }
        }

        public CustomerFullViewModel()
        {
        }

        [Display(Name = "Идентификационный номер")]
        public Guid Id { get; set; }

        [Display(Name = "Имя")] public string FirstName { get; set; }

        [Display(Name = "Фамилия")] public string LastName { get; set; }

        [Display(Name = "Адрес")] public string Email { get; set; }

        [Display(Name = "Предпочтения")] public List<string> Preferences { get; set; }
    }
}