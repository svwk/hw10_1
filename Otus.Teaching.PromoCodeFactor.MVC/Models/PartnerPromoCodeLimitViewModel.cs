﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactor.MVC.Models
{
    public class PartnerPromoCodeLimitViewModel
    {
        [Display(Name = "Дата создания")] public DateTime CreateDate { get; set; }

        [Display(Name = "Дата отмены")] public DateTime? CancelDate { get; set; }

        [Display(Name = "Дата окончания")] public DateTime EndDate { get; set; }

        [Display(Name = "Лимит")] public int Limit { get; set; }
    }
}