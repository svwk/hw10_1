﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactor.MVC.Models
{
    public class PartnerViewModel
    {
        public PartnerViewModel(Partner partner)
        {
            Id = partner.Id;
            Name = partner.Name;
            NumberIssuedPromoCodes = partner.NumberIssuedPromoCodes;
            IsActive = partner.IsActive;
        }

        public PartnerViewModel()
        {
        }

        [Display(Name = "Идентификационный номер")]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Поле '{0}' должно быть заполнено")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 50 символов")]
        [Display(Name = "Имя")]
        public string Name { get; set; }

        [Editable(false)]
        [Display(Name = "Количество выданных промокодов")]
        public int NumberIssuedPromoCodes { get; set; }

        [Display(Name = "Активен")] public bool IsActive { get; set; }
    }
}