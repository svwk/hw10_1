﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactor.MVC.Models;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Interfaces.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess;

namespace Otus.Teaching.PromoCodeFactor.MVC.Controllers
{
    public class PreferencesController : Controller
    {
        private readonly IRepository<Preference> _preferencesRepository;

        public PreferencesController(IRepository<Preference> preferencesRepository)
        {
            _preferencesRepository = preferencesRepository;
        }

        // GET: Preferences
        public async Task<IActionResult> Index()
        {
            IEnumerable<Preference> preferences;
            try
            {
                preferences = await _preferencesRepository.GetAllAsync();
                if (preferences == null) new List<Preference>();
            }
            catch (Exception)
            {
                preferences = new List<Preference>();
            }

            var response = preferences.Select(x => new PreferenceViewModel(x)).ToList();

            return View(response);
        }

        // GET: Preferences/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var preference = await _preferencesRepository.GetByIdAsync(id.Value);

            if (preference == null)
            {
                return NotFound();
            }

            var response = new PreferenceViewModel(preference);
            return View(response);
        }

        // GET: Preferences/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Preferences/Create       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(PreferenceViewModel request)
        {
            if (!ModelState.IsValid) return View(request);

            var preference = new Preference()
            {
                Name = request.Name
            };

            await _preferencesRepository.AddAsync(preference);

            return RedirectToAction(nameof(Index));
        }

        // GET: Preferences/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var preference = await _preferencesRepository.GetByIdAsync(id.Value);

            if (preference == null)
            {
                return NotFound();
            }

            return View(new PreferenceViewModel(preference));
        }

        // POST: Preferences/Edit/5       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(PreferenceViewModel request)
        {
            if (!ModelState.IsValid) return View(request);

            var preference = await _preferencesRepository.GetByIdAsync(request.Id);

            if (preference == null)
                return NotFound();

            preference.Name = request.Name;

            await _preferencesRepository.UpdateAsync(preference);

            return RedirectToAction(nameof(Index));
        }

        // GET: Preferences/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var preference = await _preferencesRepository.GetByIdAsync(id.Value);

            if (preference == null)
            {
                return NotFound();
            }

            return View(new PreferenceViewModel(preference));
        }

        // POST: Preferences/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var preference = await _preferencesRepository.GetByIdAsync(id);

            if (preference == null)
                return NotFound();

            await _preferencesRepository.DeleteAsync(preference);

            return RedirectToAction(nameof(Index));
        }

        private bool PreferenceExists(Guid id)
        {
            var preference = _preferencesRepository.GetByIdAsync(id).Result;

            return (preference != null);
        }
    }
}