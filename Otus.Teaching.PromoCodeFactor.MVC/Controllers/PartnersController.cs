﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactor.MVC.Models;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Interfaces.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Interfaces.Services;
using Otus.Teaching.PromoCodeFactory.DataAccess;

namespace Otus.Teaching.PromoCodeFactor.MVC.Controllers
{
    public class PartnersController : Controller
    {
        private readonly IRepository<Partner> _partnersRepository;

        public PartnersController(IRepository<Partner> partnersRepository)
        {
            _partnersRepository = partnersRepository;
        }

        // GET: Partners
        public async Task<IActionResult> Index()
        {
            IEnumerable<Partner> partners;
            try
            {
                partners = await _partnersRepository.GetAllAsync();
                if (partners == null) new List<Partner>();
            }
            catch (Exception)
            {
                partners = new List<Partner>();
            }

            var response = partners.Select(x => new PartnerViewModel(x));
            return View(response);
        }

        // GET: Partners/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var partner = await _partnersRepository.GetByIdAsync(id.Value);

            if (partner == null)
                return NotFound();

            var response = new PartnerFullViewModel(partner);

            return View(response);
        }

        // GET: Partners/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Partners/Create       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(PartnerViewModel partner)
        {
            //[Bind("Name,NumberIssuedPromoCodes,IsActive,Id")]
            if (!ModelState.IsValid) return View(partner);

            var newpartner = new Partner()
            {
                IsActive = partner.IsActive,
                Name = partner.Name,
                NumberIssuedPromoCodes = partner.NumberIssuedPromoCodes
            };

            await _partnersRepository.AddAsync(newpartner);

            return RedirectToAction(nameof(Index));
        }

        // GET: Partners/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var partner = await _partnersRepository.GetByIdAsync(id.Value);

            if (partner == null)
            {
                return NotFound();
            }

            return View(new PartnerViewModel(partner));
        }

        // POST: Partners/Edit/5       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(PartnerViewModel partner)
        {
            if (!ModelState.IsValid) return View(partner);

            var newpartner = await _partnersRepository.GetByIdAsync(partner.Id);

            if (newpartner == null)
                return NotFound();

            newpartner.IsActive = partner.IsActive;
            newpartner.Name = partner.Name;
            newpartner.NumberIssuedPromoCodes = partner.NumberIssuedPromoCodes;


            await _partnersRepository.UpdateAsync(newpartner);

            return RedirectToAction(nameof(Index));
        }

        // GET: Partners/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var partner = await _partnersRepository.GetByIdAsync(id.Value);

            if (partner == null)
            {
                return NotFound();
            }

            return View(new PartnerViewModel(partner));
        }

        // POST: Partners/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var partner = await _partnersRepository.GetByIdAsync(id);

            if (partner == null)
                return NotFound();

            await _partnersRepository.DeleteAsync(partner);

            return RedirectToAction(nameof(Index));
        }

        private bool PartnerExists(Guid id)
        {
            var partner = _partnersRepository.GetByIdAsync(id).Result;

            return (partner != null);
        }
    }
}